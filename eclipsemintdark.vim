" Name:    EclipseMint Dark
" Type:    Vim color scheme
" Author:  Piotr Orzechowski [orzechowski.tech]
" URL:     https://orzechowski.tech/git/orzech/eclipse-mint
" License: GNU GPL, Version 3 <https://www.gnu.org/licenses/gpl-3.0.html>
" Note:    Based on default Eclipse and Mint-X color schemes

set background=dark

hi clear
if exists("syntax_on")
    syntax reset
endif

let g:colors_name = "eclipsemintdark"

hi Normal              ctermfg=255                 guifg=Black
hi Normal              ctermbg=Black               guibg=#F7F7F7

hi Comment             ctermfg=121                 guifg=#3F7F5F

hi Constant            ctermfg=197 cterm=Bold      guifg=#7F0055 gui=Bold
hi String              ctermfg=221                 guifg=#FF8040
hi Character           ctermfg=221                 guifg=#FF8040
hi Boolean             ctermfg=197 cterm=Bold      guifg=#7F0055 gui=Bold
hi Number              ctermfg=45                  guifg=#0000C0
hi Float               ctermfg=45                  guifg=#0000C0

"Identifier (variable name)
hi Identifier          ctermfg=213 cterm=Bold      guifg=#FF0080 gui=Bold
"Function (function and method names)
hi Function            ctermfg=81                  guifg=#3F5FBF

hi Statement           ctermfg=197 cterm=Bold      guifg=#7F0055 gui=Bold
"Conditional (like switch)
"Repeat (loops)
"Label (like case, default, etc.)
"Operator (also sizeof)
"Keyword
"Exception (exception handling)

hi PreProc             ctermfg=135                 guifg=#AF5FFF
"hi PreProc             ctermfg=251                 guifg=#646464
hi Include             ctermfg=197 cterm=Bold      guifg=#7F0055 gui=Bold
"Define
"Macro
"PreCondit (preprocessor conditionals)

hi Type                ctermfg=197 cterm=Bold      guifg=#7F0055 gui=Bold
"StorageClass (type modifiers)
hi StorageClass        ctermfg=197 cterm=Bold      guifg=#7F0055 gui=Bold
hi Structure           ctermfg=197 cterm=Bold      guifg=#7F0055 gui=Bold
hi Typedef             ctermfg=197 cterm=Bold      guifg=#7F0055 gui=Bold

"Special (special symbols)
hi Special             ctermfg=117 cterm=Bold      guifg=#7F9FBF gui=Bold
"SpecialChar (special char in constant)
"Tag
"Delimiter
"SpecialComment (special string in comment)
hi SpecialComment      ctermfg=121                 guifg=#3F7F5F
"Debug

"Underlined (non-standard text and links)
hi Underlined          ctermfg=33                  guifg=#3F3FBF

"Ignore

hi Error               ctermfg=Red cterm=Bold      guifg=Red     gui=Bold
hi Error               ctermbg=LightYellow         guibg=Yellow

hi Todo                ctermfg=117 cterm=Bold      guifg=#7F9FBF gui=Bold
hi Todo                ctermbg=NONE                guibg=NONE

hi LineNr              ctermfg=249 cterm=Bold      guifg=#787878
hi ColorColumn         ctermbg=249                 guibg=#B0B4B9

hi DiffAdd             ctermfg=121                 guifg=#3F7F5F
hi DiffDelete          ctermfg=197                 guifg=#7F0055
hi DiffChange          ctermfg=221                 guifg=#FF8040
hi DiffText            ctermfg=117 cterm=Bold      guifg=#7F9FBF gui=Bold

hi Directory           ctermfg=221                 guifg=#FF8040
hi diffRemoved         ctermfg=197 cterm=Bold      guifg=Red     gui=Bold
hi diffAdded           ctermfg=121 cterm=Bold      guifg=Green   gui=Bold
hi diffChanged         ctermfg=221 cterm=Bold      guifg=#FF8040 gui=Bold
hi diffLine            ctermfg=81  cterm=Bold      guifg=#3F5FBF gui=Bold
hi diffFile            ctermfg=213 cterm=Bold      guifg=#FF0080 gui=Bold
